<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdetail extends Model
{
  protected $table = 'user_detail';
  protected $primaryKey = 'idUser';
  public $timestamps = false;

  protected $fillable = [
    'idUser', 'identityDocument', 'PosicionHCM'
  ];

}
