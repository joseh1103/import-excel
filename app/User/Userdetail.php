<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\Model;

class Userdetail extends Model
{
  protected $table = 'user_detail';
  protected $primaryKey = 'idUser';
  public $timestamps = false;

  protected $fillable = [
    'idUser', 'identityDocument', 'PosicionHCM'
  ];

  public function sexo()
  {
    return $this->hasOne('App\Entities\Sexo', 'sexoId');
  }

  public function area()
  {
    return $this->hasOne('App\Entities\Area', 'areaId');
  }

  public function department()
  {
    return $this->hasOne('App\Entities\Department', 'departmentId');
  }

  public function position()
  {
    return $this->hasOne('App\Entities\Position', 'positionId');
  }

  public function VacationDetail()
  {
    return $this->hasMany('App\Entities\Vacation\VacationDetail', 'hcmCollaborator');
  }
}
