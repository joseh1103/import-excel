<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    protected $primaryKey = 'idRole';
    public $timestamps = false;
    
    protected $fillable = [
        'idRole','roleName','modules'
    ];
}
