<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Excel;
use File;
use App\Book;
use App\Author;
use App\User;
use App\Userdetail;
use Illuminate\Support\Facades\Hash;

class BookController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function addFile()
    {
        return view('add-file');
    }

    public function showBook()
    {
        $books = DB::table('books')->paginate(10);

        return view('show-book', compact('books'));
    }

    public function showAuthor()
    {
        $authors = DB::table('authors')->paginate(10);

        return view('show-author', compact('authors'));
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ], [
            'file.required' => "Enter a file to upload!",
        ]);

        if ($request->hasFile('file')) {
            $extension = File::extension($request->file->getClientOriginalName());

            if ($extension == "xlsx" || $extension == "xls") {

                $path = $request->file->getRealPath();

                //  Load on file excel
                //$data = Excel::load($path, function($reader) {
                //})->get();

                // Load multi file excel
                $data1 = Excel::selectSheets('Sheet1')->load($path, function($reader) {
                    $data = $reader->toArray();

                    foreach ($data as $key => $value) {
                        $email = strtolower($value['mail']);
                        $userModel = User::where('email', '=', $email)->first();
                        if ($userModel) {
                          if ($userModel->save()) {
                            $userdetail = Userdetail::where('idUser', '=', $userModel->idUser)->first();
                            if ($userdetail) {
                              $userdetail->phones = $value['movil'] === 'null' ? null :json_encode($value['movil']);
                              $userdetail->divPersonal = $value['anexo'] === 'null' ? null : $value['anexo'];
                              $userdetail->save();
                            }
                          }
                        } else {
                            $userModel = new User;
                            $userModel->name = $value['name'];
                            $userModel->lastName = 'defaul';
                            $userModel->email = strtolower($value['mail']);
                            $userModel->password = Hash::make(123456);
                            $userModel->idRole = 2;
                            $userModel->idStatus = 1;
                            $userModel->avatar = null;
                  
                            if ($userModel->save()) {
                              $userdetail = new Userdetail;
                              $userdetail->idUser = $userModel->idUser;
                              $userdetail->colaborador = $value['name'];
                              $userdetail->phones = $value['movil'] === 'null' ? null : json_encode($value['movil']);
                              $userdetail->divPersonal = $value['anexo'] === 'null' ? null : $value['anexo'];
                              $userdetail->save();
                            }
                        }
                    }

                })->get();
                return back()->with('success', 'File uploaded successfully!');
            }

            // return back()->with('error', 'Error: invalid uploaded file! Enter the xlsx or xls files');
        }
	}

	public function export($type)
	{
		$book = Book::get()->toArray();
        $author = Author::get()->toArray();

		return \Excel::create('document', function($excel) use ($book, $author) {
            $excel->sheet('docsheet1', function($sheet) use ($book) {
                $sheet->fromArray($book);
            });

            $excel->sheet('docsheet2', function($sheet) use ($author) {
                $sheet->fromArray($author);
            });

        })->download($type);

	}
}
