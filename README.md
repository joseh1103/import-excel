# LaravelExcelExample

[![Build Status](https://travis-ci.org/Tony133/LaravelExcelExample.svg?branch=master)](https://travis-ci.org/Tony133/LaravelExcelExample)

Simple Example Import and Export File Excel with Laravel 5.5 LTS

## Install with Composer

```
    $ curl -s http://getcomposer.org/installer | php
    $ php composer.phar install or composer install
```

## Setting Environment

```
    $ cp .env.example .env
```

## Setting Database

```
    $ php artisan migrate
```

## App Run

```
    $ php artisan serve
```
